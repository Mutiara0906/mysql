/*No 1 Membuat Database*/
MariaDB [(none)]> create database myshop;
Query OK, 1 row affected (0.002 sec)

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| anemometer         |
| information_schema |
| myshop             |
| mysql              |
| performance_schema |
| phpmyadmin         |
| test               |
| test_arduino       |
| ujicoba            |
+--------------------+
9 rows in set (0.002 sec)

/*No 2 Membuat tabel*/
MariaDB [(none)]> use myshop
Database changed

MariaDB [myshop]> CREATE TABLE users (
    ->     id INT PRIMARY KEY AUTO_INCREMENT,
    ->     name VARCHAR (255),
    ->     email VARCHAR (255),
    ->     password VARCHAR (255));
Query OK, 0 rows affected (0.337 sec)

MariaDB [myshop]> CREATE TABLE items (
    ->     id INT PRIMARY KEY AUTO_INCREMENT,
    ->     name VARCHAR (255),
    ->     description VARCHAR (255),
    ->     prica INT,
    -> stock INT,
    ->     category_id INT,
    -> FOREIGN KEY (category_id) REFERENCES users (id));
Query OK, 0 rows affected (0.471 sec)

MariaDB [myshop]> CREATE TABLE categories (
    ->     id INT PRIMARY KEY AUTO_INCREMENT,
    ->     name VARCHAR (255));
Query OK, 0 rows affected (0.292 sec)

MariaDB [myshop]> show tables;
+------------------+
| Tables_in_myshop |
+------------------+
| categories       |
| items            |
| users            |
+------------------+
3 rows in set (0.002 sec)

MariaDB [myshop]> desc users;
+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| id       | int(11)      | NO   | PRI | NULL    | auto_increment |
| name     | varchar(255) | YES  |     | NULL    |                |
| email    | varchar(255) | YES  |     | NULL    |                |
| password | varchar(255) | YES  |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+
4 rows in set (0.248 sec)

MariaDB [myshop]> desc items;
+-------------+--------------+------+-----+---------+----------------+
| Field       | Type         | Null | Key | Default | Extra          |
+-------------+--------------+------+-----+---------+----------------+
| id          | int(11)      | NO   | PRI | NULL    | auto_increment |
| name        | varchar(255) | YES  |     | NULL    |                |
| description | varchar(255) | YES  |     | NULL    |                |
| prica       | int(11)      | YES  |     | NULL    |                |
| stock       | int(11)      | YES  |     | NULL    |                |
| category_id | int(11)      | YES  | MUL | NULL    |                |
+-------------+--------------+------+-----+---------+----------------+
6 rows in set (0.041 sec)

MariaDB [myshop]> desc categories;
+-------+--------------+------+-----+---------+----------------+
| Field | Type         | Null | Key | Default | Extra          |
+-------+--------------+------+-----+---------+----------------+
| id    | int(11)      | NO   | PRI | NULL    | auto_increment |
| name  | varchar(255) | YES  |     | NULL    |                |
+-------+--------------+------+-----+---------+----------------+
2 rows in set (0.010 sec)

/*No 3 Menginput data ke tabel yang sudah dibuat*/
/*Tabel Users*/
MariaDB [myshop]> INSERT INTO `users`(`name`, `email`, `password`) VALUES ("John Doe", "john@doe.com", "john 123");
Query OK, 1 row affected (0.086 sec)

MariaDB [myshop]> INSERT INTO `users`(`name`, `email`, `password`) VALUES ("Jane Doe", "jane@doe.com", "janeita 123");
Query OK, 1 row affected (0.074 sec)

MariaDB [myshop]> select* from users;
+----+----------+--------------+-------------+
| id | name     | email        | password    |
+----+----------+--------------+-------------+
|  1 | John Doe | john@doe.com | john 123    |
|  2 | Jane Doe | jane@doe.com | janeita 123 |
+----+----------+--------------+-------------+
2 rows in set (0.001 sec)

/*Tabel categories*/
MariaDB [myshop]> INSERT INTO `categories`(`name`) VALUES ("gadget");
Query OK, 1 row affected (0.068 sec)

MariaDB [myshop]> INSERT INTO `categories`(`name`) VALUES ("cloth");
Query OK, 1 row affected (0.063 sec)

MariaDB [myshop]> INSERT INTO `categories`(`name`) VALUES ("men");
Query OK, 1 row affected (0.055 sec)

MariaDB [myshop]> INSERT INTO `categories`(`name`) VALUES ("women");
Query OK, 1 row affected (0.042 sec)

MariaDB [myshop]> INSERT INTO `categories`(`name`) VALUES ("branded");
Query OK, 1 row affected (0.087 sec)

MariaDB [myshop]> select * from categories;
+----+---------+
| id | name    |
+----+---------+
|  1 | gadget  |
|  2 | cloth   |
|  3 | men     |
|  4 | women   |
|  5 | branded |
+----+---------+
5 rows in set (0.071 sec)

/*Tabel items*/
MariaDB [myshop]> INSERT INTO `items`(`name`, `description`, `prica`, `stock`, `category_id`) VALUES ("Unikloh","Baju keren dari merek ternama", 500000,50,2);
Query OK, 1 row affected (0.066 sec)

MariaDB [myshop]> INSERT INTO `items`(`name`, `description`, `prica`, `stock`, `category_id`) VALUES ("Unikloh","Baju keren dari merek ternama", 500000,50,2);
Query OK, 1 row affected (0.066 sec)

MariaDB [myshop]> INSERT INTO `items`(`name`, `description`, `prica`, `stock`, `category_id`) VALUES ("IMHO Watch","Jam anak yang jujur", 200000,10,1);
Query OK, 1 row affected (0.159 sec)

MariaDB [myshop]> select * from items;
+----+-------------+-------------------------------+---------+-------+-------------+
| id | name        | description                   | prica   | stock | category_id |
+----+-------------+-------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | Hape Keren dari merek sumsang | 4000000 |   100 |           1 |
|  2 | Unikloh     | Baju keren dari merek ternama |  500000 |    50 |           2 |
|  3 | IMHO Watch  | Jam anak yang jujur           |  200000 |    10 |           1 |
+----+-------------+-------------------------------+---------+-------+-------------+
3 rows in set (0.001 sec)

/*No 4 Mengambil Data dari database*/
/* a Mengambil data users */
MariaDB [myshop]> select `id`, `name`,`email` from users;
+----+----------+--------------+
| id | name     | email        |
+----+----------+--------------+
|  1 | John Doe | john@doe.com |
|  2 | Jane Doe | jane@doe.com |
+----+----------+--------------+
2 rows in set (0.001 sec)

/*b Mengambil data items*/
Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).
MariaDB [myshop]> select * from items where `prica`>1000000;
+----+-------------+-------------------------------+---------+-------+-------------+
| id | name        | description                   | prica   | stock | category_id |
+----+-------------+-------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | Hape Keren dari merek sumsang | 4000000 |   100 |           1 |
+----+-------------+-------------------------------+---------+-------+-------------+
1 row in set (0.001 sec)

Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).
MariaDB [myshop]> select * from items where name like '%sang%';
+----+-------------+-------------------------------+---------+-------+-------------+
| id | name        | description                   | prica   | stock | category_id |
+----+-------------+-------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | Hape Keren dari merek sumsang | 4000000 |   100 |           1 |
+----+-------------+-------------------------------+---------+-------+-------------+
1 row in set (0.002 sec)

/*c Menampilkan dataitems join dengan kategori*/
MariaDB [myshop]> select items.* , categories.name from items inner join categories on items.category_id = categories.id;
Empty set (0.001 sec)

MariaDB [myshop]> select items.*, categories.* from items, categories where items.category_id = categories.id;
Empty set (0.001 sec)

/* No 5 Mengubah Data dari Database */
MariaDB [myshop]> update items set prica = 2500000  where name = 'Sumsang b50';
Query OK, 1 row affected (0.252 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [myshop]> select * from items;
+----+-------------+-------------------------------+---------+-------+-------------+
| id | name        | description                   | prica   | stock | category_id |
+----+-------------+-------------------------------+---------+-------+-------------+
|  1 | Sumsang b50 | Hape Keren dari merek sumsang | 2500000 |   100 |           1 |
|  2 | Unikloh     | Baju keren dari merek ternama |  500000 |    50 |           2 |
|  3 | IMHO Watch  | Jam anak yang jujur           |  200000 |    10 |           1 |
+----+-------------+-------------------------------+---------+-------+-------------+
3 rows in set (0.001 sec)
